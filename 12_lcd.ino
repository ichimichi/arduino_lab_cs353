#include<LiquidCrystal.h>

int val = 0;
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

void setup()
{
 lcd.begin(16, 2);  // initializes the 16x2 LCD
}

void loop()
{
  lcd.setCursor(0,0);  //sets the cursor at row 0 column 0
  lcd.print("HAPHI "); // prints 16x2 LCD MODULE
  lcd.setCursor(0,1);  //sets the cursor at row 1 column 0
  lcd.print(val);
  delay(1000);
  val = val +1;
}