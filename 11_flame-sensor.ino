int buzzer = 11;
int flameSensor = 12;
int noFlameDetected = LOW;


void setup()
{
    Serial.begin(9600);
    pinMode(buzzer, OUTPUT);
    pinMode(flameSensor , INPUT);
}

void loop()
{
    noFlameDetected = digitalRead(flameSensor);
    Serial.println(noFlameDetected);
    if ( noFlameDetected == HIGH)
    {
        Serial.println("No flame Detected");
        digitalWrite(buzzer,LOW );
    }
    else
    {
        Serial.println("Flame Detected");
        digitalWrite(buzzer, HIGH);
    }
    
    delay(1000);
}