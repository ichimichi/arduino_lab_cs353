#include <dht.h>

int DHTpin = 8 ;

dht DHT;

void setup()
{
  Serial.begin(9600);
  pinMode(DHTpin, INPUT);
}

void loop()
{
  delay(2000);
  int a = DHT.read11(DHTpin);
  Serial.println("Temperature : ");
  Serial.println(DHT.temperature);
  delay(1000);
  Serial.println("Humidity : ");
  Serial.println(DHT.humidity);
}