// https://learn.adafruit.com/pir-passive-infrared-proximity-motion-sensor/using-a-pir-w-arduino

int inputPin = 2;
int state = LOW;             // we start, assuming no motion detected
int motionDetected = 0;

void setup() {
  pinMode(inputPin, INPUT);     // declare sensor as input
  Serial.begin(9600);
}
 
void loop(){
  motionDetected = digitalRead(inputPin);
  if (motionDetected == HIGH) { 
    if ( state == LOW) {
      // we have just turned on
      Serial.println("Motion detected!");
      // We only want to print on the output change, not state
       state = HIGH;
    }
  } else {
    if ( state == HIGH){
      // we have just turned of
      Serial.println("Motion ended!");
      // We only want to print on the output change, not state
       state = LOW;
    }
  }
}