// http://henrysbench.capnfatz.com/henrys-bench/arduino-sensors-and-input/arduino-ir-obstacle-sensor-tutorial-and-manual/

int isObstaclePin = 7;  // This is our input pin
int isObstacle = LOW;  // LOW MEANS NO OBSTACLE

void setup() {
  Serial.begin(9600);
  pinMode(isObstaclePin, INPUT);
}

void loop() {
  isObstacle = digitalRead(isObstaclePin);
  
  if (isObstacle == HIGH)
  {
    Serial.println("OBSTACLE");
  }
  else
  {
    Serial.println("NO OBSTACLE");
  }
  delay(200);
}