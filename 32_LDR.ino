// https://www.instructables.com/id/LDR-Sensor-Module-Users-Manual-V10/

int intensity;

void setup()
{
    Serial.begin(9600);
}

void loop()
{
    intensity = analogRead(A0);
    if(intensity >= 800){
        Serial.println("Dark");
    }
     else if(intensity >=650)
    {
        Serial.println("Dim");
    }
    else if(intensity >=400)
    {
        Serial.println("Normal");
    }
    else if(intensity >=200)
    {
        Serial.println("Bright");
    }
    else if(intensity >=100)
    {
        Serial.println("Brighter");
    }
    else if(intensity >=0)
    {
        Serial.println("Ultra Bright");
    }
    
}