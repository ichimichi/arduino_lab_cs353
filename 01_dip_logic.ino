const int in_switch_1 = 2;
const int in_switch_2 = 3;
const int select_4 = 4;
const int select_5 = 5;
const int select_6 = 6;
const int out_led_1 =  13;    


int switchState_1 = LOW;
int switchState_2 = LOW;
int sv_4 = LOW;
int sv_5 = LOW;
int sv_6 = LOW;


void setup() {
  pinMode(out_led_1, OUTPUT);
  pinMode(in_switch_1, INPUT);
  pinMode(in_switch_2, INPUT);
  pinMode(select_4, INPUT);
  pinMode(select_5, INPUT);
  pinMode(select_6, INPUT);
}

void loop() {
  switchState_1 = digitalRead(in_switch_1);
  switchState_2 = digitalRead(in_switch_2);
  sv_4 = digitalRead(select_4);
  sv_5 = digitalRead(select_5);
  sv_6 = digitalRead(select_6);

  // OR
  if(sv_4==LOW && sv_5 == LOW && sv_6==LOW){
    if (switchState_1 == HIGH || switchState_2 == HIGH) {
    digitalWrite(out_led_1, HIGH);
    } else {
    digitalWrite(out_led_1, LOW);
    }
  }

  if(sv_4==LOW && sv_5 == LOW && sv_6==HIGH){
    // AND
    if (switchState_1 == HIGH && switchState_2 == HIGH) {
      digitalWrite(out_led_1, HIGH);
    } else {
      digitalWrite(out_led_1, LOW);
    }
  }

  if(sv_4==LOW && sv_5 == HIGH && sv_6==LOW){
      // NOR
    if (switchState_1 == HIGH || switchState_2 == HIGH) {
      digitalWrite(out_led_1, LOW);
    } else {
      digitalWrite(out_led_1, HIGH);
    }
  }

  if(sv_4==LOW && sv_5 == HIGH && sv_6==HIGH){
      // NAND
    if (switchState_1 == HIGH && switchState_2 == HIGH) {
      digitalWrite(out_led_1, LOW);
    } else {
      digitalWrite(out_led_1, HIGH);
    }
  }

  if(sv_4==HIGH && sv_5 == LOW && sv_6== LOW){
    // XOR
    if ((switchState_1 == HIGH && switchState_2 == LOW)||switchState_1 == LOW && switchState_2 == HIGH) {
      digitalWrite(out_led_1, HIGH);
    } else {
      digitalWrite(out_led_1, LOW);
    }
  }

  if(sv_4==HIGH && sv_5 == LOW && sv_6== HIGH){
    // XNOR
    if ((switchState_1 == HIGH && switchState_2 == LOW)||switchState_1 == LOW && switchState_2 == HIGH) {
      digitalWrite(out_led_1, LOW);
    } else {
      digitalWrite(out_led_1, HIGH);
    }
  }


}