// https://randomnerdtutorials.com/guide-for-microphone-sound-sensor-with-arduino/

int ledPin=13;
int sensorPin=7;
boolean soundDetected = 0;

void setup(){
  pinMode(ledPin, OUTPUT);
  pinMode(sensorPin, INPUT);
  Serial.begin (9600);
}
  
void loop (){
  soundDetected =digitalRead(sensorPin);
  // when the sensor detects a signal above the threshold soundDetectedue, LED flashes
  if (soundDetected==HIGH) {
    Serial.println ("Sound Detected");
    digitalWrite(ledPin, HIGH);
  }
  else {
    Serial.println ("No Sound Detected");
    digitalWrite(ledPin, LOW);
  }
}